def swap_Bubble_sort_increase(a, b):
    if a[1] > b[1]:
        a, b = b, a
    return a, b


def swap_Bubble_sort_decrease(a, b):
    if a[1] < b[1]:
        a, b = b, a
    return a, b


# Пузырьковая сортировка
def Bubble_sort(A, reverse=False):
    if type(A) not in [list]:
        raise TypeError('the value should be a list')
    var = 1
    for _ in range(len(A) - 1):
        for i in range(len(A) - var):
            if not reverse:
                # на возрастание
                A[i], A[i + 1] = swap_Bubble_sort_increase(A[i], A[i + 1])
            else:
                # на убывание
                A[i], A[i + 1] = swap_Bubble_sort_decrease(A[i], A[i + 1])
        var += 1
    return A


def swap_Sort_by_inserts_increase(a, b, i):
    if a[1] < b[1]:
        a, b = b, a
        i -= 1
        if i == 0:
            return a, b, i, False
    else:
        return a, b, i, False
    return a, b, i, True


def swap_Sort_by_inserts_decrease(a, b, i):
    if a[1] > b[1]:
        a, b = b, a
        i -= 1
        if i == 0:
            return a, b, i, False
    else:
        return a, b, i, False
    return a, b, i, True


# Сортировка вставками
def Sort_by_inserts(A, reverse=False):
    if type(A) not in [list]:
        raise TypeError('the value should be a list')
    for i in range(1, len(A)):
        while True:
            if not reverse:
                # на возрастание
                A[i], A[i - 1], i, var = swap_Sort_by_inserts_increase(A[i], A[i - 1], i)
                if not var:
                    break
            else:
                # на убывание
                A[i], A[i - 1], i, var = swap_Sort_by_inserts_decrease(A[i], A[i - 1], i)
                if not var:
                    break
    return A


def swap_Shell_sort_increase(a, b):
    if a[1] < b[1]:
        a, b = b, a
    return a, b


def swap_Shell_sort_decrease(a, b):
    if a[1] > b[1]:
        a, b = b, a
    return a, b


# Сортировка Шелла
def Shell_sort(A, reverse=False):
    if type(A) not in [list]:
        raise TypeError('the value should be a list')
    step = len(A) // 2
    while True:
        for i in range(step, len(A)):
            while i - step >= 0:
                if not reverse:
                    # на возрастание
                    A[i], A[i - step] = swap_Shell_sort_increase(A[i], A[i - step])
                else:
                    # на убывание
                    A[i], A[i - step] = swap_Shell_sort_decrease(A[i], A[i - step])
                i -= step
        step = step // 2
        if step == 0:
            break
    return A


def swap_Quick_sort_increase(a, M, i, id_M):
    if a[1] < M or (a[1] == M and i != id_M and i < id_M):
        return True
    elif a[1] > M or (a[1] == M and i != id_M and i > id_M):
        return False
    else:
        return None


def swap_Quick_sort_decrease(a, M, i, id_M):
    if a[1] > M or (a[1] == M and i != id_M and i < id_M):
        return True
    elif a[1] < M or (a[1] == M and i != id_M and i > id_M):
        return False
    else:
        return None


# Быстрая сортировка
def Quick_sort(A, reverse=False):
    if type(A) not in [list]:
        raise TypeError('the value should be a list')
    if len(A) <= 1:
        return A
    else:
        M, L, R = A[len(A) // 2][1], [], []
        for i in range(len(A)):
            if not reverse:
                # на возрастание
                var = swap_Quick_sort_increase(A[i], M, i, len(A) // 2)
                if var is not None:
                    if var:
                        L.append(A[i])
                    else:
                        R.append(A[i])
            else:
                # на убывание
                var = swap_Quick_sort_decrease(A[i], M, i, len(A) // 2)
                if var is not None:
                    if var:
                        L.append(A[i])
                    else:
                        R.append(A[i])
        return Quick_sort(L, reverse) + [A[len(A) // 2]] + Quick_sort(R, reverse)

